import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './Pokemon.module.css';

class Pokemon extends Component {
  state = {
    typeSelected: `${styles.cardContainer} `
  };

  componentDidMount() {
    const { pokemon } = this.props;
    this.setState({
      typeSelected:
        `${styles.cardContainer} ` + this.getClass(pokemon.types[0].type.name)
    });
  }

  getClass(type) {
    switch (type) {
      case 'fire':
        return styles.fireType;
      case 'electric':
        return styles.electricType;
      case 'water':
        return styles.waterType;
      case 'grass':
        return styles.grassType;
      case 'poison':
        return styles.poisonType;
      case 'flying':
        return styles.flyingType;
      case 'bug':
        return styles.bugType;
      case 'normal':
        return styles.normalType;
      case 'psychic':
        return styles.psychicType;
      case 'dragon':
        return styles.dragonType;
      default:
        return '';
    }
  };

  handleTypeChange(type) {
    this.setState({
      ...this.state,
      typeSelected: `${styles.cardContainer} ` + this.getClass(type)
    });
  }

  render() {
    const { pokemon } = this.props;

    return (
      <div className={this.state.typeSelected} data-testid='Pokemon'>
        <div>
          <p className={styles.cardTitle}>#</p>
          <p>{pokemon.id}</p>
        </div>
        <div>
          <p className={styles.cardTitle}>Name:</p>
          <p>{pokemon.name}</p>
        </div>
        <div>
          <p className={styles.cardTitle}>Images:</p>
          <br />
          {pokemon.sprites.front_default && (
            <img
              className={styles.cardImage}
              src={pokemon.sprites.front_default}
              alt='back_default'
            />
          )}
          {pokemon.sprites.back_default && (
            <img
              className={styles.cardImage}
              src={pokemon.sprites.back_default}
              alt='front_default'
            />
          )}
        </div>
        <div>
          <p className={styles.cardtitle}>Types</p>
          <div>
            {pokemon.types.map(element => {
              return (
                <p
                  key={element.slot}
                  onClick={() => this.handleTypeChange(element.type.name)}
                  className={styles.cardType}
                  data-testid='cardType'
                >
                  {element.type.name}
                </p>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

Pokemon.propTypes = {
  pokemon: PropTypes.object,
};

export default Pokemon;
