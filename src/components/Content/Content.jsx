import React from 'react';
import PropTypes from 'prop-types';

import Pokemon from '../Pokemon/Pokemon';

import styles from './Content.module.css';

const Content = ({ pokemonList }) => {
  const showPokemons = () => {
    return pokemonList
      .slice(0)
      .reverse()
      .map(pokemon => {
        return <Pokemon key={pokemon.id} pokemon={pokemon} />;
      });
  };

  return (
    <div className={`col-8 col-sm-10 offset-4 offset-sm-2 h-100 bg-secondary text-white py-2 d-flex fixed-top overflow-auto ${styles.content}`} data-testid='Content'>
      <div>{showPokemons()}</div>
    </div>
  );
};

Content.propTypes = {
  pokemonList: PropTypes.array,
};

export default Content;
