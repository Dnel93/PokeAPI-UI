import React from 'react';
import { render, fireEvent } from '../test-utils'

import Leftbar from '../../../components/Leftbar/Leftbar';
import * as actions from '../../../redux/actions/pokemon-service-actions';

it('Should render component properly', () => {
  const { getByTestId } = render(<Leftbar />);
  expect(getByTestId('Leftbar')).toBeTruthy();
});

it('Should render PokeAPI header properly', () => {
  const { getByTestId } = render(<Leftbar />);
  expect(getByTestId('Leftbar-Header')).toBeTruthy();
});

it('Should handle onSubmit properly', () => {
  const { getByTestId } = render(<Leftbar />);
  fireEvent.submit(getByTestId('Leftbar-Form'));
  expect(getByTestId('Leftbar-Form')).toBeTruthy();
});

it('Should handle keyDown (anything but ENTER) properly', () => {
  const { getByTestId } = render(<Leftbar />);
  fireEvent.keyDown(getByTestId('Leftbar-Input'), { key: 'A', code: 'KeyA' });
  expect(getByTestId('Leftbar-Input')).toBeTruthy();
});

// TODO: update validation
xit('Should handle keyDown (ENTER) properly', () => {
  const { getByTestId } = render(<Leftbar getPokemon={actions.getPokemon('pikachu')} />, {
    initialState: {
      pokemonList: [],
      isLoading: false,
      error: ''
    }
  });
  fireEvent.keyDown(getByTestId('Leftbar-Input'), {key: 'Enter', keyCode: 13, charCode: 13});
  expect(getByTestId('Leftbar-Input')).toBeTruthy();
});

describe('External Components', () => {
  it('Should render Spinner properly', () => {
    const { getByTestId } = render(<Leftbar isLoading />);
    expect(getByTestId('Spinner')).toBeTruthy();
  });

  it('Should render Alert properly', () => {
    const { getByTestId } = render(<Leftbar error='An error has ocurred' />, {
      initialState: {
        pokemonList: [],
        isLoading: false,
        error: ''}});

    expect(getByTestId('Leftbar')).toBeTruthy();
    expect(getByTestId('Alert-ErrorMessage')).toBeTruthy();
  });
});
