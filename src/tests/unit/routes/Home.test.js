import React from 'react';
import { render } from '../test-utils';

import Home from '../../../routes/Home/Home';

// TODO: Update test to work with Redux
xit('Should render component properly', () => {
  const { getByTestId } = render(<Home />, {
    initialState: {
      pokemonList: [],
      isLoading: false,
      error: ''}});
  expect(getByTestId('Home')).toBeTruthy();
});