import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';

import * as actions from '../../../../redux/actions/pokemon-service-actions';
import * as types from '../../../../redux/actions/types';

jest.mock('axios');

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it('Should return GET_POKEMON__SUCCESS properly', () => {
  const initialState = {
    pokemonServiceReducer : {
      pokemonList: [],
      isLoading: false,
      error: ''
    }
  }

  const pokemon = {
    id: 25,
    name: 'pikachu',
    sprites: {
      back_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/25.png',
      front_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png'
    },
    types: [
      {
        slot: 1,
        type: {
          name: 'electric'
        }
      }
    ]
  };

  const store = mockStore(initialState);

  const expectedActions = [
    { type: types.GET_POKEMON },
    { type: types.GET_POKEMON__SUCCESS, payload: { ...pokemon } }
  ];

  axios.get.mockReturnValue({data: pokemon});

  return store.dispatch(actions.getPokemon('pikachu')).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

it('Should return GET_POKEMON__ALREADY_EXIST properly filtered by name', () => {
  const pokemon = {
    id: 25,
    name: 'pikachu',
    sprites: {
      back_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/25.png',
      front_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png'
    },
    types: [
      {
        slot: 1,
        type: {
          name: 'electric'
        }
      }
    ]
  };

  const initialState = {
    pokemonServiceReducer: {
      pokemonList: [{...pokemon}],
      isLoading: false,
      error: ''
    }
  }

  const store = mockStore(initialState);

  const expectedActions = [
    { type: types.GET_POKEMON },
    { type: types.GET_POKEMON__ALREADY_EXIST }
  ];

  return store.dispatch(actions.getPokemon('pikachu')).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  })
});

it('Should return GET_POKEMON__ALREADY_EXIST properly filtered by number', () => {
  const pokemon = {
    id: 25,
    name: 'pikachu',
    sprites: {
      back_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/25.png',
      front_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png'
    },
    types: [
      {
        slot: 1,
        type: {
          name: 'electric'
        }
      }
    ]
  };

  const initialState = {
    pokemonServiceReducer: {
      pokemonList: [{ ...pokemon }],
      isLoading: false,
      error: ''
    }
  }

  const store = mockStore(initialState);

  const expectedActions = [
    { type: types.GET_POKEMON },
    { type: types.GET_POKEMON__ALREADY_EXIST }
  ];

  return store.dispatch(actions.getPokemon('25')).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  })
});

it('Should return GET_POKEMON__FAILURE properly', () => {
  const error = {message: 'Network Error'};

  const initialState = {
    pokemonServiceReducer: {
      pokemonList: [],
      isLoading: false,
      error: ''
    }
  };

  const store = mockStore(initialState);

  axios.get.mockReturnValue(Promise.reject(error));

  const expectedActions = [
    { type: types.GET_POKEMON },
    { type: types.GET_POKEMON__FAILURE, payload: error.message}
  ];

  return store.dispatch(actions.getPokemon('asd')).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

it('Should close the alert component', () => {
  const initialState= {};
  const store = mockStore(initialState);

  store.dispatch(actions.closeAlert());

  const storeActions = store.getActions();
  const expectedResponse = { type: types.ALERT__CLOSED};

  expect(storeActions).toEqual([expectedResponse]);
});
