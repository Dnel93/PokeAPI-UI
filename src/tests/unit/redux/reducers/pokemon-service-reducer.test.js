import reducer from '../../../../redux/reducers/pokemon-service-reducer';
import * as types from '../../../../redux/actions/types';

describe('pokemon-service-reducer', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined,{})).toEqual({
      pokemonList: [],
      isLoading: false,
      error: ''
    });
  });

  it('should return isLoading as TRUE when GET_POKEMON gets fried', () => {
    const action = {
      type: types.GET_POKEMON
    };

    const result = {
      pokemonList: [],
      isLoading: true,
      error: ''
    };

    expect(reducer(undefined,action)).toEqual(result);
  });

  it('should return error message and isLoading false when GET_POKEMON__FAILURE gets fired', () => {
    const errorMessage = 'An error has ocurred'
    const action = {
      type: types.GET_POKEMON__FAILURE,
      payload: errorMessage
    }

    const result = {
      pokemonList: [],
      error: errorMessage,
      isLoading: false
    };

    expect(reducer(undefined, action)).toEqual(result);
  });

  it('Should return a new pokemon in the list, and isLoading should be false when GET_POKEMON__SUCCESS gets fired', () => {
    const pokemon = {
      id: 25,
      name: 'pikachu',
      sprites: {
        back_default:
          'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/25.png',
        front_default:
          'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png'
      },
      types: [
        {
          slot: 1,
          type: {
            name: 'electric'
          }
        }
      ]
    };

    const action = {
      type: types.GET_POKEMON__SUCCESS,
      payload: pokemon
    };

    const result = {
      pokemonList: [pokemon],
      error: '',
      isLoading: false
    };

    expect(reducer(undefined, action)).toEqual(result);
  });

  it('Should return isLoading false when GET_POKEMON__ALREADY_EXIST gets fired', () => {
    const action = {
      type: types.GET_POKEMON__ALREADY_EXIST
    };

    const result = {
      pokemonList: [],
      error: '',
      isLoading: false
    };

    expect(reducer(undefined, action)).toEqual(result);
  });

  it('Should return an empty error once ALERT__CLOSED gets fired', () => {
    const action = {
      type: types.ALERT__CLOSED
    };

    const result = {
      pokemonList: [],
      error: '',
      isLoading: false
    };

    expect(reducer(undefined, action)).toEqual(result);
  })


});